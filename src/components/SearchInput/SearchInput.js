import { InputGroup, FormControl } from 'react-bootstrap';

function SearchInput({ onChange }) {
  return (
    <InputGroup onChange={onChange} >
      <InputGroup.Prepend bg='light'>
        <InputGroup.Text id='inputGroup-sizing-default'>
          <i className='fa fa-search' aria-hidden='true'></i>
        </InputGroup.Text>
      </InputGroup.Prepend>
      <FormControl
        aria-label='Default'
        aria-describedby='inputGroup-sizing-default'
      />
    </InputGroup>
  );
}

export default SearchInput;
