import { Navbar, Container } from 'react-bootstrap'
 
function Header() {
  return (
    <header>
        <Navbar className='navbar-custom' variant='dark'>
          <Container className='justify-content-center'>
            <Navbar.Brand>Contacts</Navbar.Brand>
          </Container>
        </Navbar>
      </header>
  )
}

export default Header
