import { useState } from 'react';
import { ListGroup, InputGroup, Image } from 'react-bootstrap';
import smilePng from '../../assets/smile.png';

function User({ data }) {
  const { id, first_name, last_name, avatar } = data;

  const [checkedUsers, setCheckedUsers] = useState([]);
  const checked = [];

  const handleCheckOnChange = (e) => {
    if (e.target.checked) {
      checked.push(e.target.name);
      console.log(checked);
      setCheckedUsers([...checkedUsers, [e.target.name]]);
    } else {
      setCheckedUsers([...checkedUsers]);
    }
  };

  return (
    <ListGroup.Item className='d-flex align-items-center'>
      <Image src={avatar ? avatar : smilePng} fluid roundedCircle thumbnail />
      <div className='mx-2'>
        <p className='m-0'>
          {first_name} {last_name}
        </p>
      </div>
      <div style={{ marginLeft: 'auto', cursor: 'pointer' }}>
        <InputGroup.Checkbox onChange={(e) => handleCheckOnChange(e)} name={id} />
      </div>
    </ListGroup.Item>
  );
}

export default User;
