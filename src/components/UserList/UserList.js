import { ListGroup } from 'react-bootstrap';

function UserList({ children }) {
  return (
    <ListGroup variant='flush'>
      { children }
    </ListGroup>
  );
}

export default UserList;
