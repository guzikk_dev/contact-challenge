import { useEffect, useState } from 'react';
import axios from 'axios';

import Header from '../components/Header/Header';
import SearchInput from '../components/SearchInput/SearchInput';
import UserList from '../components/UserList/UserList';
import User from '../components/User/User';
import { Spinner, Alert, Container, Row, Col } from 'react-bootstrap';

function ContactPage() {
  const [users, setUsers] = useState([]);
  const [filteredUsers, setFilteredUsers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchUsers();
  }, [users]);

  const fetchUsers = async () => {
    try {
      const { data } = await axios.get('http://localhost:5000/users');
      setUsers(data);
      setLoading(false);
    } catch (err) {
      setError('Something went wrong');
      setLoading(false);
    }
  };

  const sortUsersByLastName = (array) => {
    const sorted = array.sort((a, b) => a.last_name.localeCompare(b.last_name));
    return sorted;
  };

  const filterUsers = (e) => {
    const value = e.target.value.toLowerCase().trim();

    if (value === '') {
      setFilteredUsers([]);
      return;
    }

    const filtered = users.filter(
      (user) =>
        user.first_name.toLowerCase().includes(value) ||
        user.last_name.toLowerCase().includes(value)
    );

    setFilteredUsers(filtered);
  };

  return (
    <>
      <Header />
      <Container>
        <SearchInput onChange={filterUsers} />
        {loading && (
          <Row>
            <Col className='text-center my-3'>
              <Spinner animation='border' role='status'>
                <span className='sr-only'>Loading...</span>
              </Spinner>
            </Col>
          </Row>
        )}
        {filteredUsers.length ? (
          <UserList>
            {sortUsersByLastName(filteredUsers).map((user) => (
              <User key={user.id} data={user} />
            ))}
          </UserList>
        ) : (
          <UserList>
            {sortUsersByLastName(users).map((user) => (
              <User key={user.id} data={user} />
            ))}
          </UserList>
        )}
        {error && (
          <Row>
            <Col md={{ span: 4, offset: 4 }}>
              <Alert variant='danger' className='text-center my-4'>
                {error}
              </Alert>
            </Col>
          </Row>
        )}
      </Container>
    </>
  );
}

export default ContactPage;
