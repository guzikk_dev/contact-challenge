# Download project

### You can make clone by run 'git clone https://gitlab.com/guzikk_dev/contact-challenge.git'

### Or simply download the project from repository

# Getting Started with Create React App

### Navigate into root folder and run 'npm install'

### Run 'npm start' to run react app

# Getting Started with the server

### Navigate into server folder and run npm install

### Run 'node index.js' to run server
