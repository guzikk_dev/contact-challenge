require('dotenv').config();
const express = require('express');
const cors = require('cors');
const axios = require('axios');
const PORT = process.env.PORT || 5001;

const app = express();

app.use(cors());

app.get('/users', async (req, res) => {
  try {
    const response = await axios.get(process.env.JSON_URI);
    const users = await response.data;

    res.status(200).json(users)
  } catch (err) {
    res.status(500).json({
      error: 'Something went wrong, please try again'
    })
  }
})


app.listen(PORT, () => {
  console.log(`Server is starting on port ${PORT}`);
});